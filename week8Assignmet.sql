create database Bookes;

CREATE TABLE `bookes`.`user` (
  `user_id` INT NOT NULL,
  `user_name` VARCHAR(200) NULL,
  `user_email` VARCHAR(200) NULL,
  `user_password` VARCHAR(200) NULL,
  PRIMARY KEY (`user_id`));

CREATE TABLE `bookes`.`book` (
  `book_id` INT NOT NULL,
  `book_name` VARCHAR(200) NULL,
  `book_author` VARCHAR(200) NULL,
  `book_genre` VARCHAR(200) NULL,
  PRIMARY KEY (`book_id`));

CREATE TABLE `bookes`.`readlater` (
  `read_later_id` INT NOT NULL,
  `book_id` INT NULL,
  `user_id` INT NULL,
  `book_name` VARCHAR(200) NULL,
  `author` VARCHAR(200) NULL,
  PRIMARY KEY (`read_later_id`),
  INDEX `book_id_idx` (`book_id` ASC, `user_id` ASC) VISIBLE,
  INDEX `user_id_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `book_id`
    FOREIGN KEY (`book_id` , `user_id`)
    REFERENCES `bookes`.`book` (`book_id` , `book_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `bookes`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);





INSERT INTO `bookes`.`book` (`book_id`, `book_name`, `book_author`, `book_price`) VALUES ('2', 'dhoom', 'akash', '100');
INSERT INTO `bookes`.`book` (`book_id`, `book_name`, `book_author`, `book_price`) VALUES ('3', 'chand', 'srad ', '300');
INSERT INTO `bookes`.`book` (`book_id`, `book_name`, `book_author`, `book_price`) VALUES ('4', 'raat', 'yogeh', '500');
INSERT INTO `bookes`.`book` (`book_id`, `book_name`, `book_author`, `book_price`) VALUES ('5', 'din', 'shivani', '200');
