<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bookes</title>
<!-- font awesome link-->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
body {
	background-image:url("assets/Books.jpg");
	background-color: #FOF8FF;
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}
</style>
</head>
<body>
	<nav class="navbar navbar-dark justify-content-between">
		<h1 class="btn btn-outline-info my-2 my-sm-0">Bookes</h1>
		<form class="form-inline">
			<button class="btn btn-outline-info my-2 my-sm-0">
				<a style="text-decoration: none;" class="text-info" href="bookList">Show
					Book List</a>
			</button>
		</form>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-12">
					<h1 style="color: #8A2BE2">
						Welcome to the online book manager.
						<h1>

				<h2 style="color:blue">User manual</h2>
				<ol type="1" style="font-size: 20px; color: #OOFFFF">
					 <li>Click on booklist so you can get all the book data.</li>
			      <li>Click on register so you can register yourself .</li>
			      <li>Click on login so you can login.</li>
			      <li>Click on likedBook so you can get your liked book data.</li>
			      <li>Click on read later so you can  read book later .</li>
			      <li>Click on logout so you can logout fron book manager.</li>
				</ol>
			</div>
		</div>
	</div>
</body>
</html>