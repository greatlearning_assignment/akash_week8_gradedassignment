<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<style>
body {
	background-image:url("assets/Books.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}
</style>
</head>
<body>

	<nav class="navbar navbar-dark justify-content-between">
		<h1 class="btn btn-outline-info my-2 my-sm-0">Register Here</h1>
	</nav>
	<div class='container pt-5 pb-5'>
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-6 col-lg-offset-3">
				<form action="register" method="post"
					class="bg-info pt-5 pb-5 pl-5 pr-5 border border-dark rounded">
				    <%
						String msg = (String) request.getAttribute("error");
						if (msg != null) {
					%>
					<h4 class='text-center text-warning'><%=msg%></h4>
					<%
						}
					%>
				    
					<h3 class='text-center'>Please Enter Details</h3>
					<div class="form-group">
						<label for="id"><h4>Id</h4></label> <input type="text"
							class="form-control" id="id" placeholder="Enter id"
							name="id">
						<label for="name"><h4>Name</h4></label> <input type="text"
							class="form-control" id="name" placeholder="Enter name"
							name="name">
					</div>
					<div class="form-group">
						<label for="username"><h4>Email</h4></label> <input type="text"
							class="form-control" id="email" placeholder="Enter email"
							name="email">
					</div>
					<div class="form-group">
						<label for="password"><h4>Password</h4></label> <input
							type="password" class="form-control" id="password"
							placeholder="Enter Password" name="password">
					</div>

					<div align="center">
						<button type="submit"
							class="btn btn-outline-info my-2 my-sm-0 bg-dark" name="register"
							value="Register">Register</button>
					</div>
				</form>
			</div>
			<div class="col-lg-3"></div>
		</div>
	</div>
</body>
</html>