package com.springmvc.demo.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.springmvc.demo.model.Book;
import com.springmvc.demo.model.LikedBook;

@Repository
public class LikedBookDatabase {
	@Autowired
	private JdbcTemplate template;

	public List<Book> getAllUserLikedBook(int userId) throws SQLException {
		String sql = "select book.book_id,book.book_name,book_author,book.book_price" + " from"
		+ " likebook" + " left join book ON(book.book_id = likebook.book_id)"
		+ " left join user ON(user.id = likebook.user_id)" + " Where user.id = "+userId;
		return this.template.query(sql, new RowMapper<Book>() {
			public Book mapRow(ResultSet rs, int arg1) throws SQLException {
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setName(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setPrice(rs.getDouble(4));

				return book;
			}
		});
	}

	public boolean likedBookValidation(LikedBook likedBook) {
		for (LikedBook likedB : getAllLikedBooks()) {
			if (likedBook.getBookId() == likedB.getBookId() && likedBook.getUserId() == likedB.getUserId()) {
				return true;
			}
		}
		return false;
	}

	// This function is used for insert book into read later list
	public boolean insertLikedBook(LikedBook likedBook) throws SQLException {
		// checking book is already in list
		if (likedBookValidation(likedBook)) {
			return false;
		} else {
			// Preparing query and execute it
			String insertQuery = "insert into likebook(book_id,user_id) values(?,?)";

			int count = template.update(insertQuery, likedBook.getBookId(), likedBook.getUserId());
			return true;

		}
	}

	public List<LikedBook> getAllLikedBooks() {
		String sql = "select * from likebook";
		return this.template.query(sql, new LikedBookRowMapper() {
			public LikedBook mapRow(ResultSet rs, int arg1) throws SQLException {
				LikedBook likedBook = new LikedBook();
				likedBook.setId(rs.getInt(1));
				likedBook.setBookId(rs.getInt(2));
				likedBook.setUserId(rs.getInt(3));
				return likedBook;
			}
		});
	}

	class LikedBookRowMapper implements RowMapper<LikedBook> {
		public LikedBook mapRow(ResultSet rs, int arg1) throws SQLException {
			// System.out.println(arg1);
			LikedBook likedBook = new LikedBook();
			likedBook.setId(rs.getInt(1));
			likedBook.setBookId(rs.getInt(2));
			likedBook.setUserId(rs.getInt(3));

			return likedBook;
		}
	}
}
