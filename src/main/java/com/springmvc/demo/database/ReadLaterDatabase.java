package com.springmvc.demo.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.springmvc.demo.model.Book;
import com.springmvc.demo.model.ReadLater;

@Repository
public class ReadLaterDatabase {
	@Autowired
	private JdbcTemplate template;

	// This function is used for getting the list of read later books
	public List<Book> getAllLaterReadBook(int userId) throws SQLException {
		// Preparing query and execute it
		String sql = "select book.book_id,book.book_name,book.book_author,book.book_price" + " from"
				+ " readlater" + " left join tbl_book ON(book.book_id = readlater.book_id)"
				+ " left join user ON(user.user_id = readlater.user_id)" + " Where user.user_id = "
				+ userId;

		return this.template.query(sql, new RowMapper<Book>() {
			public Book mapRow(ResultSet rs, int arg1) throws SQLException {
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setName(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setPrice(rs.getDouble(4));

				return book;
			}
		});
	}

	public boolean readBookValidation(ReadLater readBook) {

		for (ReadLater readB : getAllReadBooks()) {
			if (readBook.getBookId() == readB.getBookId() && readBook.getUserId() == readB.getUserId()) {
				return true;
			}
		}
		return false;
	}

	// This function is used for insert book into read later list
	public boolean insertReadLater(ReadLater readBook) throws SQLException {
		// checking book is already in list

		if (readBookValidation(readBook)) {
			return false;
		} else {
			String insertQuery = "insert into readlater(book_id,user_id) values(?,?)";

			int count = template.update(insertQuery, readBook.getBookId(), readBook.getUserId());
			return true;

		}
	}

	public List<ReadLater> getAllReadBooks() {
		String sql = "select * from readlater";
		return this.template.query(sql, new ReadBookRowMapper() {
			public ReadLater mapRow(ResultSet rs, int arg1) throws SQLException {
				ReadLater readBooks = new ReadLater();
				readBooks.setId(rs.getInt(1));
				readBooks.setBookId(rs.getInt(2));
				readBooks.setUserId(rs.getInt(3));
				return readBooks;
			}
		});
	}

	class ReadBookRowMapper implements RowMapper<ReadLater> {
		public ReadLater mapRow(ResultSet rs, int arg1) throws SQLException {
			// System.out.println(arg1);
			ReadLater readBook = new ReadLater();
			readBook.setId(rs.getInt(1));
			readBook.setBookId(rs.getInt(2));
			readBook.setUserId(rs.getInt(3));

			return readBook;
		}
	}
}

