package com.springmvc.demo.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.springmvc.demo.model.Book;



@Repository
public class BookDatabase {

	@Autowired
	private JdbcTemplate template;

	public List<Book> getAllBook() {
		String sql = "select * from book";

		return this.template.query(sql, new RowMapper<Book>() {
			public Book mapRow(ResultSet rs, int arg1) throws SQLException {
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setName(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setPrice(rs.getDouble(4));

				return book;
			}
		});
	}

	class BookRowMapper implements RowMapper<Book> {
		public Book mapRow(ResultSet rs, int arg1) throws SQLException {

			Book book = new Book();

			book.setId(rs.getInt(1));
			book.setName(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setPrice(rs.getDouble(4));

			return book;
		}
	}

}
