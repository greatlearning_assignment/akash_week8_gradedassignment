package com.springmvc.demo.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.springmvc.demo.model.User;

@Repository
public class UserDatabase {

	@Autowired
	private JdbcTemplate template;

	public boolean userValidation(User user) {
		for (User loginUser : getAllUser()) {
			if (loginUser.getPassword().equals(user.getPassword())) {
				return true;
			}
		}
		return false;
	}

	public List<User> getAllUser() {
		String sql = "select * from user";
		return this.template.query(sql, new RowMapper<User>() {
			public User mapRow(ResultSet rs, int arg1) throws SQLException {
				User user = new User();
				user.setId(rs.getInt(1));
				user.setName(rs.getString(2));
				user.setEmail(rs.getString(3));
				user.setPassword(rs.getString(4));
				return user;
			}
		});
	}

	public boolean insertUser(User user) {

		String sql = "insert into user(user_id,user_name,user_email,user_password) values(?,?,?,?)";
		for (User loginUser : getAllUser()) {
			if (loginUser.getEmail().equals(user.getEmail())) {
				return false;
			}
		}
		int count = template.update(sql,user.getId(), user.getName(), user.getEmail(), user.getPassword());
		return true;
	}

	public User getUserNameByEmail(String email) {
		String sql = "select * from user where user_email = ?";
		return this.template.queryForObject(sql, new UserRowMapper(), email);

	}

	class UserRowMapper implements RowMapper<User> {
		public User mapRow(ResultSet rs, int arg1) throws SQLException {
			// System.out.println(arg1);
			User user = new User();
			user.setId(rs.getInt(1));
			user.setName(rs.getString(2));
			user.setEmail(rs.getString(3));
			user.setPassword(rs.getString(4));
			return user;
		}
	}

}

