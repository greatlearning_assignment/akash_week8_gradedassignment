package com.springmvc.demo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springmvc.demo.database.BookDatabase;
import com.springmvc.demo.model.Book;


@Controller
public class BookController {
	@Autowired
	private BookDatabase bookDatabase;

	@GetMapping("/bookList")
	public String getBookList(Map<String, List<Book>> map){
		try {
			List<Book> books = bookDatabase.getAllBook();
			map.put("books", books);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to find the book lsit!");
			e.printStackTrace();
		}
		return "bookList";
	}
	
	@GetMapping("/dashboard")
	public String getDashboard(@RequestParam(required = false) String error,Map<String, List<Book>> map,Map<String, String> msg, HttpSession httpSession){
		try {
			List<Book> books = bookDatabase.getAllBook();
			map.put("books", books);
			if(error != null) {
				System.out.println(error);
				msg.put("error", error);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to find the book lsit!");
			e.printStackTrace();
		}
		return "dashboard";
	}
}
