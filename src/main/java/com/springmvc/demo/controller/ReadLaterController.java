package com.springmvc.demo.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springmvc.demo.database.ReadLaterDatabase;
import com.springmvc.demo.model.Book;
import com.springmvc.demo.model.ReadLater;

@Controller
public class ReadLaterController {
	@Autowired
	private ReadLaterDatabase readBookDatabase;

	@GetMapping("/readLater")
	public String getReadLaterBook(Map<String, List<Book>> map, HttpSession httpSession) {
		int userId = (Integer) httpSession.getAttribute("userId");
		List<Book> readBooks;
		try {
			readBooks = readBookDatabase.getAllLaterReadBook(userId);
			System.out.println(readBooks);
			if(!readBooks.isEmpty()) {
				map.put("readLaterBooks", readBooks);
				return "readLaterList";
			}else {
				return "redirect:dashboard?error=You have empty read later List!";
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:dashboard?error=You have empty read later List!";

	}

	@GetMapping("/read")
	public String insertReadLaterBook(@RequestParam int bookId, HttpSession httpSession) {
		int userId =  (Integer) httpSession.getAttribute("userId");
		ReadLater readBook = new ReadLater(bookId,userId);
		try {
			if (readBookDatabase.insertReadLater(readBook)) {
				return "redirect:dashboard?error=You have successfuly added book in read later list!";
			} else {
				return "redirect:dashboard?error=You have already this book in read later list!";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:dashboard?error=You can'not add this book in read later book list!";
		
	}
}

