package com.springmvc.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springmvc.demo.database.UserDatabase;
import com.springmvc.demo.model.User;

@Controller
public class UserController {
	@Autowired
	private UserDatabase userDatabase;

	@GetMapping("/login")
	public String getLogin(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "login";
	}

	@PostMapping("/login")
	public String postLogin(@RequestParam(required = false) String register, User user, HttpSession httpSession) {

		if (register == null) {
			try {
				if (userDatabase.userValidation(user)) {
					httpSession.setAttribute("userId", userDatabase.getUserNameByEmail(user.getEmail()).getId());
					httpSession.setAttribute("name", userDatabase.getUserNameByEmail(user.getEmail()).getName());
					return "redirect:dashboard";
				} else {

					return "redirect:login?error=invalid creditionals";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return "redirect:register?error=not registered";

			}
		} else {
			return "redirect:register";
		}
	}

	@GetMapping("/register")
	public String getRegisterPage(@RequestParam(required = false) String error, Map<String, String> map) {
		if (error != null) {
			map.put("error", error);
		}
		return "register";
	}

	@PostMapping("/register")
	public String userRegister(User user) {
		try {
			if (userDatabase.insertUser(user)) {
				return "redirect:login?error= You have successfuly registered!";
			} else {
				System.out.println("not in validation part");

				return "redirect:register?error=You have already registered!";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "redirect:register?error=not registered";

		}
	}

	@GetMapping("/logout")
	public String userLogout(HttpSession httpSession) {
		httpSession.removeAttribute("userId");
		httpSession.removeAttribute("name");
		return "redirect:login";

	}

}
