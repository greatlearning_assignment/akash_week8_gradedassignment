package com.springmvc.demo.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.springmvc.demo.database.LikedBookDatabase;
import com.springmvc.demo.model.Book;
import com.springmvc.demo.model.LikedBook;

@Controller
public class LikedBookController {
	@Autowired
	private LikedBookDatabase likedBookDatabase;

	@GetMapping("/likedBook")
	public String getReadLaterBook(Map<String, List<Book>> map, HttpSession httpSession) {
		int userId = (Integer) httpSession.getAttribute("userId");
		List<Book> likedBooks;
		try {
			likedBooks = likedBookDatabase.getAllUserLikedBook(userId);
			if(!likedBooks.isEmpty()) {
				map.put("likedBookList", likedBooks);
				return "likedBookList";
			}else {
				return "redirect:dashboard?error=You have empty liked book List!";
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:dashboard?error=You have empty liked book List!";

	}

	@GetMapping("/likedBookInsert")
	public String insertReadLaterBook(@RequestParam int bookId, HttpSession httpSession) {
		int userId = (Integer) httpSession.getAttribute("userId");
		LikedBook likedBook = new LikedBook(bookId, userId);
		try {
			if (likedBookDatabase.insertLikedBook(likedBook)) {
				return "redirect:dashboard?error=You have successfuly added book in liked book list!";
			} else {
				return "redirect:dashboard?error=You have already this book in liked book list!";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:dashboard?error=You can'not add this book in liked book list!";
	}

}
