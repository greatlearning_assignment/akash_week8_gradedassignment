package com.springmvc.demo.model;

import org.springframework.stereotype.Component;

@Component
public class ReadLater {
	private int id;
	private int bookId;
	private int userId;

	public ReadLater() {

	}

	public ReadLater(int id, int bookId, int userId) {
		this.id = id;
		this.bookId = bookId;
		this.userId = userId;
	}

	public ReadLater(int bookId, int userId) {
		super();
		this.bookId = bookId;
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "ReadBook [id=" + id + ", bookId=" + bookId + ", userId=" + userId + "]";
	}

}

